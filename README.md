Board module
============
Module to manage bord items etc


Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Edit your composer.json

```
    "repositories": [
...
        {
            "type": "git",
            "url": "https://xtetis@bitbucket.org/xtetis/yii2-board.git"
        }
...
    ]
```

Either run

```
php composer.phar require --prefer-dist xtetis/yii2-board "dev-master"
```

or add

```
"xtetis/yii2-board":"dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
    'modules' => [
...
        'board' => [
            'class' => 'xtetis\board\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
...
    ],
```


Migrations
-----
```
    ./yii migrate/up --migrationPath=@vendor/xtetis/yii2-board/src/migrations
```



# Модуль для управления доской объявлений
Составляет из себя модуль для работы с доской объялений и с возможностью добавления объявлений со стороны как зарегистрированных пользователей, так и гостей сайта
В составе:
- Модели для достаупа к нужным таблицам
- Модули для управления данными в базе
- Миграция для создания необходимых таблиц в базе