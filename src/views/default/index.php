<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\BoardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Объявления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php /*= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_user',
            'id_district',
            'id_city',
            //'id_board_category',
            //'id_currency',
            'name',
            //'about:ntext',
            //'price',
            //'create_date',
            //'pub_date',
            //'source',
            //'active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */ ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="list-view">
    <?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_list',
]);
?>







            </ul>
        </div>
    </div>

</div>
