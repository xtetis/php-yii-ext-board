<?php
    use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<!--div class="news-item">
    <h2><?=Html::encode($model->name)?></h2>
    <?=HtmlPurifier::process($model->about)?>
</div-->


<li>
    <div class="card list-view-media">
        <div class="card-block">
            <div class="media">
                <a class="media-left"
                   href="/board/item/<?=$model->id?>">
                    <img class="media-object card-list-img"
                         src="/theme/templates/adminty/files/assets/images/avatar-3.jpg"
                         alt="Generic placeholder image">
                </a>
                <div class="media-body">
                    <div class="f-13 text-muted m-b-15">
                        <a class="lead m-t-0"
                           href="/board/item/<?=$model->id?>">
                            <?=Html::encode($model->name)?>
                        </a>
                    </div>
                    <p>Hi, This is my short intro text.
                        Lorem ipsum is a dummy content sit
                        dollar. You can copy and paste this
                        dummy content from anywhere and to
                        anywhere. Its all free and must be a
                        good to folllow in prectice</p>
                    <div class="m-t-15">
                        <button type="button"
                                data-toggle="tooltip"
                                title=""
                                class="btn btn-facebook btn-mini waves-effect waves-light"
                                data-original-title="Facebook">
                            <span class="icofont icofont-social-facebook"></span>
                        </button>
                        <button type="button"
                                data-toggle="tooltip"
                                title=""
                                class="btn btn-twitter btn-mini waves-effect waves-light"
                                data-original-title="Twitter">
                            <span class="icofont icofont-social-twitter"></span>
                        </button>
                        <button type="button"
                                data-toggle="tooltip"
                                title=""
                                class="btn btn-linkedin btn-mini waves-effect waves-light"
                                data-original-title="Linkedin">
                            <span class="icofont icofont-brand-linkedin"></span>
                        </button>
                        <button type="button"
                                data-toggle="tooltip"
                                title=""
                                class="btn btn-dribbble btn-mini waves-effect waves-light"
                                data-original-title="Drible">
                            <span class="icofont icofont-social-dribble"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>
