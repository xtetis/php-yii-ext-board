<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use xtetis\currency\models\Currency;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

    $district_options    = ArrayHelper::map($districts, 'id', 'name');
    $district_options[0] = '---';
    asort($district_options);

    $this->title                   = Yii::t('app', 'Добавить объявление');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Объявления'), 'url' => ['/board']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-create">

    <h1><?=Html::encode($this->title)?></h1>

    <div class="board-add-form">
        <?php $form = ActiveForm::begin(
            [
                'options' => ['enctype' => 'multipart/form-data'],
                //'enableAjaxValidation' => true,
            ]
            );?>



        <div class="form-group field-board-id_city">
            <label class="control-label"
                   for="board-id_city">Регион/город</label>
            <div class="input-group input-group-button">
                <input type="text"
                       class="form-control"
                       readonly="readonly"
                       id="name_id_city"
                       placeholder="Укажите город"
                       value="<?=$name_id_city?>">
                <span class="input-group-addon btn btn-primary btn-sm"
                      id="basic-addon3"
                      data-toggle="modal"
                      data-target="#modal__select_id_city">
                    <span class="">Выбрать</span>
                </span>
            </div>
            <?=$form->field($model, 'id_city')->hiddenInput()->label(false)?>
        </div>


        <?php Pjax::begin(['id' => 'id_district_container']);?>
        <div class="<?=($board_add['id_city'] ? 'hasid' : 'noid')?>">
            <?=Html::a('Обновить', ['/board/add', 'id_city' => $board_add['id_city']], ['style' => 'display:none;', 'id' => 'btn_refresh_id_district_block'])?>
            <?php echo $form->field($model, 'id_district')->dropDownList($district_options); ?>
        </div>
        <?php Pjax::end();?>

        <div class="form-group field-board-id_board_category">
            <label class="control-label"
                   for="board-id_board_category">Категория объявления</label>
            <div class="input-group input-group-button">
                <input type="text"
                       class="form-control"
                       readonly="readonly"
                       id="name_id_board_category"
                       placeholder="Укажите категорию"
                       value="<?=$name_id_board_category?>">
                <span class="input-group-addon btn btn-primary btn-sm"
                      id="basic-addon3"
                      data-toggle="modal"
                      data-target="#modal__select_id_board_category">
                    <span class="">Выбрать</span>
                </span>
            </div>
            <?=$form->field($model, 'id_board_category')->hiddenInput()->label(false)?>
        </div>


        <?=$form->field($model, 'name')->textInput(['maxlength' => true])?>
        <?=$form->field($model, 'about')->textarea(['rows' => 6])?>

        <div class="row">
            <div class="col-sm">
                <?=$form->field($model, 'price')->textInput()?>
            </div>
            <div class="col-sm">
                <?=$form->field($model, 'id_currency')->dropDownList(ArrayHelper::map(Currency::find()->all(), 'id', 'name'));?>
            </div>
        </div>




        <?=\xtetis\image\widgets\Gallery::widget(['model' => $modelAlbum,]);?>

        <?php if (Yii::$app->user->isGuest): ?>

            <?= $form->field($modelUser, 'email') ?>
            <?= $form->field($modelUser, 'username') ?>
            
        <?php endif; ?>

        


        <div class="form-group">
            <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
        </div>

        <?php ActiveForm::end();?>

    </div>



    <div style="display:none;"
         class="tooltips_data">
        <div idx="board-name">
            <div class="ttitle">Напишите заголовок</div>
            <div class="tcontent">
                Введите подробный и понятный заголовок. Избегайте слов с ЗАГЛАВНЫМИ БУКВАМИ</div>
        </div>
        <div idx="board-about">
            <div class="ttitle">Опишите товар или услугу</div>
            <div class="tcontent">
                <ul class='b_add_tooltip_list'>
                    <li>Укажите преимущества вашего товара или услуги.</li>
                    <li>Предоставьте как можно больше подробностей </li>
                </ul>
            </div>
        </div>
        <div idx="board-price">
            <div class="ttitle">Укажите цену</div>
            <div class="tcontent">
                Установите корректную цену на ваш товар или услугу. <br>
                Оставьте поле пустым, если цена договорная.
            </div>
        </div>
        <div idx="name_id_board_category">
            <div class="ttitle">Выберите категорию</div>
            <div class="tcontent">
                Укажите рубрику, к которой относится ваше объявление
            </div>
        </div>
        <div idx="name_id_city">
            <div class="ttitle">Выберите город</div>
            <div class="tcontent">
                Укажите область и город для которых создаете объявление
            </div>
        </div>
        <div idx="board-id_district">
            <div class="ttitle">Укажите район</div>
            <div class="tcontent">
                Если хотите, чтобы объявление соответствовало только одному району
            </div>
        </div>




    </div>








    <?php
        $bc_level[0]      = [];
        $bc_level[1]      = [];
        $bc_level[2]      = [];

        $bc_ids     = [];
        $bc_parents = [];

        foreach ($board_categories as $board_category)
        {
            if (0 == $board_category->id_parent)
            {
                $bc_level[0][] = $board_category->id;
            }
            $bc_ids[]     = $board_category->id;
            $bc_parents[] = $board_category->id_parent;
        }

        foreach ($board_categories as $board_category)
        {
            if (in_array($board_category->id_parent, $bc_level[0]))
            {
                $bc_level[1][] = $board_category->id;
            }
        }

        foreach ($board_categories as $board_category)
        {
            if (in_array($board_category->id_parent, $bc_level[1]))
            {
                $bc_level[2][] = $board_category->id;
            }
        }

    ?>

    <!-- Modal -->
    <div class="modal fade  bd-example-modal-lg"
         id="modal__select_id_board_category"
         tabindex="-1"
         role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog  modal-lg"
             role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"
                        id="exampleModalLongTitle">Выберите категорию</h5>
                    <button type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="accordion">

                        <?php for ($i = 0; $i <= 2; $i++)
                            {
                            ?>
                        <div class="card card_level_<?=$i?>">
                            <div class="card-header"
                                 id="heading<?=$i?>">
                                <div class="mb-0">
                                    <button class="btn btn-link btn-sm"
                                            data-toggle="collapse"
                                            data-target="#collapse<?=$i?>"
                                            aria-expanded="false"
                                            aria-controls="collapse<?=$i?>">
                                        Рубрика
                                    </button>
                                </div>
                            </div>

                            <div id="collapse<?=$i?>"
                                 class="collapse <?=((!$i) ? 'show' : '')?> "
                                 aria-labelledby="heading<?=$i?>"
                                 data-parent="#accordion">
                                <div class="card-body">

                                    <?php foreach ($board_categories as $board_category)
                                            {
                                            ?>
                                    <?php if (in_array($board_category->id, $bc_level[$i]))
            {
            ?>
                                    <div idx="<?=$board_category->id?>"
                                         has_child="<?=((in_array($board_category->id, $bc_parents)) ? 1 : 0)?>"
                                         level="<?=$i?>"
                                         parent="<?=$board_category->id_parent?>"
                                         class="bc_level bc_level_<?=$i?> bc_id_<?=$board_category->id?> bc_parent_<?=$board_category->id_parent?>">
                                        <?=$board_category->name?>
                                    </div>
                                    <?php }?>
                                    <?php }?>

                                </div>
                            </div>
                        </div>
                        <?}?>
                    </div>





                </div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
















    <!-- Modal -->
    <div class="modal fade  bd-example-modal-lg"
         id="modal__select_id_city"
         tabindex="-1"
         role="dialog"
         aria-hidden="true">
        <div class="modal-dialog  modal-lg"
             role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Выберите область/город</h5>
                    <button type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="accordion_region">

                        <div class="card card_region">
                            <div class="card-header"
                                 id="heading_region">
                                <div class="mb-0">
                                    <button class="btn btn-link btn-sm"
                                            data-toggle="collapse"
                                            data-target="#collapse_region"
                                            aria-expanded="false"
                                            aria-controls="collapse_region">
                                        Области/Регионы
                                    </button>
                                </div>
                            </div>

                            <div id="collapse_region"
                                 class="collapse show"
                                 aria-labelledby="heading_region"
                                 data-parent="#accordion_region">
                                <div class="card-body">

                                    <?php foreach ($regions as $region)
                                        {
                                        ?>
                                    <div idx="<?=$region->id?>"
                                         class="region_item region_item_<?=$region->id?>">
                                        <?=$region->name?>
                                    </div>
                                    <?php }?>

                                </div>
                            </div>
                        </div>



                        <div class="card card_city">
                            <div class="card-header"
                                 id="heading_city">
                                <div class="mb-0">
                                    <button class="btn btn-link btn-sm"
                                            data-toggle="collapse"
                                            data-target="#collapse_city"
                                            aria-expanded="false"
                                            aria-controls="collapse_city">
                                        Города
                                    </button>
                                </div>
                            </div>

                            <div id="collapse_city"
                                 class="collapse"
                                 aria-labelledby="heading_city"
                                 data-parent="#accordion_region">
                                <div class="card-body">

                                    <?php foreach ($cities as $city)
                                        {
                                        ?>
                                    <div idx="<?=$city->id?>"
                                         parent="<?=$city->id_region?>"
                                         class="city_item city_item_parent_<?=$city->id_region?>">
                                        <?=$city->name?>
                                    </div>
                                    <?php }?>

                                </div>
                            </div>
                        </div>

                    </div>





                </div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>


</div>
