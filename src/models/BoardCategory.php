<?php

namespace xtetis\board\models;

use Yii;

/**
 * This is the model class for table "{{%board_category}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_parent Родительская категория
 * @property string $name Название категории
 * @property int $active Активность записи
 *
 * @property Board[] $boards
 * @property BoardOptionInCategory[] $boardOptionInCategories
 */
class BoardCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%board_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_parent', 'active'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_parent' => 'Родительская категория',
            'name' => 'Название категории',
            'active' => 'Активность записи',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoards()
    {
        return $this->hasMany(Board::className(), ['id_board_category' => 'id'])->inverseOf('boardCategory');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardOptionInCategories()
    {
        return $this->hasMany(BoardOptionInCategory::className(), ['id_board_category' => 'id'])->inverseOf('boardCategory');
    }
}
