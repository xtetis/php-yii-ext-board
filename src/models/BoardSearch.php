<?php

namespace xtetis\board\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use xtetis\board\models\Board;

/**
 * BoardSearch represents the model behind the search form of `app\models\Board`.
 */
class BoardSearch extends Board
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_district', 'id_city', 'id_board_category', 'id_currency', 'price', 'active'], 'integer'],
            [['name', 'about', 'create_date', 'pub_date', 'source'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Board::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_district' => $this->id_district,
            'id_city' => $this->id_city,
            'id_board_category' => $this->id_board_category,
            'id_currency' => $this->id_currency,
            'price' => $this->price,
            'create_date' => $this->create_date,
            'pub_date' => $this->pub_date,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'about', $this->about])
            ->andFilterWhere(['like', 'source', $this->source]);

        return $dataProvider;
    }
}
