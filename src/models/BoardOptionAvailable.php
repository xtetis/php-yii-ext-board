<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%board_option_available}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_board_option Ссылка на опцию
 * @property int $id_parent Родительский элемент
 * @property string $name Имя
 *
 * @property BoardOption $boardOption
 */
class BoardOptionAvailable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%board_option_available}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_board_option'], 'required'],
            [['id_board_option', 'id_parent'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['id_board_option'], 'exist', 'skipOnError' => true, 'targetClass' => BoardOption::className(), 'targetAttribute' => ['id_board_option' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_board_option' => 'Ссылка на опцию',
            'id_parent' => 'Родительский элемент',
            'name' => 'Имя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardOption()
    {
        return $this->hasOne(BoardOption::className(), ['id' => 'id_board_option'])->inverseOf('boardOptionAvailables');
    }
}
