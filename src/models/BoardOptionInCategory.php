<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%board_option_in_category}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_board_option Ссылка на опцию
 * @property int $id_board_category Ссылка на категорию
 * @property int $use_setting Использовать свойство в категории
 * @property int $use_filter Использовать фильтрацию в категории
 *
 * @property BoardCategory $boardCategory
 * @property BoardOption $boardOption
 */
class BoardOptionInCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%board_option_in_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_board_option', 'id_board_category'], 'required'],
            [['id_board_option', 'id_board_category', 'use_setting', 'use_filter'], 'integer'],
            [['id_board_category'], 'exist', 'skipOnError' => true, 'targetClass' => BoardCategory::className(), 'targetAttribute' => ['id_board_category' => 'id']],
            [['id_board_option'], 'exist', 'skipOnError' => true, 'targetClass' => BoardOption::className(), 'targetAttribute' => ['id_board_option' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_board_option' => 'Ссылка на опцию',
            'id_board_category' => 'Ссылка на категорию',
            'use_setting' => 'Использовать свойство в категории',
            'use_filter' => 'Использовать фильтрацию в категории',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardCategory()
    {
        return $this->hasOne(BoardCategory::className(), ['id' => 'id_board_category'])->inverseOf('boardOptionInCategories');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoardOption()
    {
        return $this->hasOne(BoardOption::className(), ['id' => 'id_board_option'])->inverseOf('boardOptionInCategories');
    }
}
