<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%board}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%district}}`
 * - `{{%city}}`
 * - `{{%region}}`
 * - `{{%board_category}}`
 * - `{{%currency}}`
 */
class m190404_202630_create_board_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%board}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_district' => $this->integer()->notNull(),
            'id_city' => $this->integer()->notNull(),
            'id_board_category' => $this->integer()->notNull(),
            'id_currency' => $this->integer()->notNull(),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'about' => $this->text()->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'price' => $this->integer()->defaultValue(0),
            'create_date' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')),
            'pub_date' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')),
            'source' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'active' => $this->integer()->defaultValue(1),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // creates index for column `id_user`
        $this->createIndex(
            '{{%idx-board-id_user}}',
            '{{%board}}',
            'id_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-board-id_user}}',
            '{{%board}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_city`
        $this->createIndex(
            '{{%idx-board-id_city}}',
            '{{%board}}',
            'id_city'
        );

        // add foreign key for table `{{%city}}`
        $this->addForeignKey(
            '{{%fk-board-id_city}}',
            '{{%board}}',
            'id_city',
            '{{%city}}',
            'id',
            'CASCADE'
        );


        // creates index for column `id_board_category`
        $this->createIndex(
            '{{%idx-board-id_board_category}}',
            '{{%board}}',
            'id_board_category'
        );

        // add foreign key for table `{{%board_category}}`
        $this->addForeignKey(
            '{{%fk-board-id_board_category}}',
            '{{%board}}',
            'id_board_category',
            '{{%board_category}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_currency`
        $this->createIndex(
            '{{%idx-board-id_currency}}',
            '{{%board}}',
            'id_currency'
        );

        // add foreign key for table `{{%currency}}`
        $this->addForeignKey(
            '{{%fk-board-id_currency}}',
            '{{%board}}',
            'id_currency',
            '{{%currency}}',
            'id',
            'CASCADE'
        );

       
        $this->addCommentOnColumn('{{%board}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%board}}','id_user', 'Ссылка на пользователя');
        $this->addCommentOnColumn('{{%board}}','id_district', 'Ссылка на район');
        $this->addCommentOnColumn('{{%board}}','id_city', 'Ссылка на город');
        $this->addCommentOnColumn('{{%board}}','id_board_category', 'Ссылка категорию объявления');
        $this->addCommentOnColumn('{{%board}}','id_currency', 'Ссылка валюту');
        $this->addCommentOnColumn('{{%board}}','name', 'Заголовок объявления');
        $this->addCommentOnColumn('{{%board}}','about', 'Текст объявления');
        $this->addCommentOnColumn('{{%board}}','price', 'Цена');
        $this->addCommentOnColumn('{{%board}}','create_date', 'Дата создания');
        $this->addCommentOnColumn('{{%board}}','pub_date', 'Дата публикации');
        $this->addCommentOnColumn('{{%board}}','source', 'Источник объявления');
        $this->addCommentOnColumn('{{%board}}','active', 'Активность объявления');

        $this->addCommentOnTable('{{%board}}','Список объявлений');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-board-id_user}}',
            '{{%board}}'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            '{{%idx-board-id_user}}',
            '{{%board}}'
        );

        // drops foreign key for table `{{%district}}`
        $this->dropForeignKey(
            '{{%fk-board-id_district}}',
            '{{%board}}'
        );

        // drops index for column `id_district`
        $this->dropIndex(
            '{{%idx-board-id_district}}',
            '{{%board}}'
        );

        // drops foreign key for table `{{%city}}`
        $this->dropForeignKey(
            '{{%fk-board-id_city}}',
            '{{%board}}'
        );

        // drops index for column `id_city`
        $this->dropIndex(
            '{{%idx-board-id_city}}',
            '{{%board}}'
        );

        // drops foreign key for table `{{%board_category}}`
        $this->dropForeignKey(
            '{{%fk-board-id_board_category}}',
            '{{%board}}'
        );

        // drops index for column `id_board_category`
        $this->dropIndex(
            '{{%idx-board-id_board_category}}',
            '{{%board}}'
        );

        // drops foreign key for table `{{%currency}}`
        $this->dropForeignKey(
            '{{%fk-board-id_currency}}',
            '{{%board}}'
        );

        // drops index for column `id_currency`
        $this->dropIndex(
            '{{%idx-board-id_currency}}',
            '{{%board}}'
        );

        $this->dropTable('{{%board}}');
    }
}
