<?php

use yii\db\Migration;

/**
 * Handles adding id_album to table `{{%board}}`.
 */
class m190425_172959_add_id_album_column_to_board_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%board}}', 'id_album', $this->integer()->defaultValue(0));
        $this->addCommentOnColumn('{{%board}}','id_album', 'Ссылка на альбом (image)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%board}}', 'id_album');
    }
}
