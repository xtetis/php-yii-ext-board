<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%board_option_available}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%board_option}}`
 */
class m190403_210424_create_board_option_available_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%board_option_available}}', [
            'id' => $this->primaryKey(),
            'id_board_option' => $this->integer()->notNull(),
            'id_parent' => $this->integer()->defaultValue(0),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // creates index for column `id_board_option`
        $this->createIndex(
            '{{%idx-board_option_available-id_board_option}}',
            '{{%board_option_available}}',
            'id_board_option'
        );

        // add foreign key for table `{{%board_option}}`
        $this->addForeignKey(
            '{{%fk-board_option_available-id_board_option}}',
            '{{%board_option_available}}',
            'id_board_option',
            '{{%board_option}}',
            'id',
            'CASCADE'
        );

        $this->addCommentOnColumn('{{%board_option_available}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%board_option_available}}','id_board_option', 'Ссылка на опцию');
        $this->addCommentOnColumn('{{%board_option_available}}','id_parent', 'Родительский элемент');
        $this->addCommentOnColumn('{{%board_option_available}}','name', 'Имя');

        $this->addCommentOnTable('{{%board_option_available}}','Список допустимых значений для опций объявлений');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%board_option}}`
        $this->dropForeignKey(
            '{{%fk-board_option_available-id_board_option}}',
            '{{%board_option_available}}'
        );

        // drops index for column `id_board_option`
        $this->dropIndex(
            '{{%idx-board_option_available-id_board_option}}',
            '{{%board_option_available}}'
        );

        $this->dropTable('{{%board_option_available}}');
    }
}
