<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%board_option}}`.
 */
class m190403_063739_create_board_option_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%board_option}}', [
            'id' => $this->primaryKey(),
            'option_type' => $this->integer()->defaultValue(0),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'description' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'introtext' => $this->string(300)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'filter_type' => $this->integer()->defaultValue(1),
            'active' => $this->integer()->defaultValue(1),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addCommentOnColumn('{{%board_option}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%board_option}}','option_type', 'Тип элемента ввода (0 - select, 1 - text input)');
        $this->addCommentOnColumn('{{%board_option}}','name', 'Название категории');
        $this->addCommentOnColumn('{{%board_option}}','description', 'Краткое описание опции');
        $this->addCommentOnColumn('{{%board_option}}','introtext', 'Подробное описание опции');
        $this->addCommentOnColumn('{{%board_option}}','filter_type', 'ХЗ');
        $this->addCommentOnColumn('{{%board_option}}','active', 'Активность записи');

        $this->addCommentOnTable('{{%board_option}}','Список опций объявлений');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%board_option}}');
    }
}
