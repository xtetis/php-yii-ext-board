<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%board_abuse}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%board}}`
 * - `{{%user}}`
 */
class m190405_194306_create_board_abuse_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%board_abuse}}', [
            'id' => $this->primaryKey(),
            'id_board' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'message' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'create_date' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // creates index for column `id_board`
        $this->createIndex(
            '{{%idx-board_abuse-id_board}}',
            '{{%board_abuse}}',
            'id_board'
        );

        // add foreign key for table `{{%board}}`
        $this->addForeignKey(
            '{{%fk-board_abuse-id_board}}',
            '{{%board_abuse}}',
            'id_board',
            '{{%board}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_user`
        $this->createIndex(
            '{{%idx-board_abuse-id_user}}',
            '{{%board_abuse}}',
            'id_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-board_abuse-id_user}}',
            '{{%board_abuse}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );


        $this->addCommentOnColumn('{{%board_abuse}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%board_abuse}}','id_user', 'Ссылка на пользователя');
        $this->addCommentOnColumn('{{%board_abuse}}','id_board', 'Ссылка на объявление');
        $this->addCommentOnColumn('{{%board_abuse}}','message', 'Текст жалобы');
        $this->addCommentOnColumn('{{%board_abuse}}','create_date', 'Дата создания');

        $this->addCommentOnTable('{{%board_abuse}}','Список жалоб на объявления');



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%board}}`
        $this->dropForeignKey(
            '{{%fk-board_abuse-id_board}}',
            '{{%board_abuse}}'
        );

        // drops index for column `id_board`
        $this->dropIndex(
            '{{%idx-board_abuse-id_board}}',
            '{{%board_abuse}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-board_abuse-id_user}}',
            '{{%board_abuse}}'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            '{{%idx-board_abuse-id_user}}',
            '{{%board_abuse}}'
        );

        $this->dropTable('{{%board_abuse}}');
    }
}
