<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%board_option_in_category}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%board_option}}`
 * - `{{%board_category}}`
 */
class m190403_212724_create_board_option_in_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%board_option_in_category}}', [
            'id' => $this->primaryKey(),
            'id_board_option' => $this->integer()->notNull(),
            'id_board_category' => $this->integer()->notNull(),
            'use_setting' => $this->integer()->defaultValue(0),
            'use_filter' => $this->integer()->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // creates index for column `id_board_option`
        $this->createIndex(
            '{{%idx-board_option_in_category-id_board_option}}',
            '{{%board_option_in_category}}',
            'id_board_option'
        );

        // ad foreign key for table `{{%board_option}}`
        $this->addForeignKey(
            '{{%fk-board_option_in_category-id_board_option}}',
            '{{%board_option_in_category}}',
            'id_board_option',
            '{{%board_option}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_board_category`
        $this->createIndex(
            '{{%idx-board_option_in_category-id_board_category}}',
            '{{%board_option_in_category}}',
            'id_board_category'
        );

        // ad foreign key for table `{{%board_category}}`
        $this->addForeignKey(
            '{{%fk-board_option_in_category-id_board_category}}',
            '{{%board_option_in_category}}',
            'id_board_category',
            '{{%board_category}}',
            'id',
            'CASCADE'
        );

        $this->addCommentOnColumn('{{%board_option_in_category}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%board_option_in_category}}','id_board_option', 'Ссылка на опцию');
        $this->addCommentOnColumn('{{%board_option_in_category}}','id_board_category', 'Ссылка на категорию');
        $this->addCommentOnColumn('{{%board_option_in_category}}','use_setting', 'Использовать свойство в категории');
        $this->addCommentOnColumn('{{%board_option_in_category}}','use_filter', 'Использовать фильтрацию в категории');

        $this->addCommentOnTable('{{%board_option_in_category}}','Список допустимых значений для опций объявлений');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%board_option}}`
        $this->dropForeignKey(
            '{{%fk-board_option_in_category-id_board_option}}',
            '{{%board_option_in_category}}'
        );

        // drops index for column `id_board_option`
        $this->dropIndex(
            '{{%idx-board_option_in_category-id_board_option}}',
            '{{%board_option_in_category}}'
        );

        // drops foreign key for table `{{%board_category}}`
        $this->dropForeignKey(
            '{{%fk-board_option_in_category-id_board_category}}',
            '{{%board_option_in_category}}'
        );

        // drops index for column `id_board_category`
        $this->dropIndex(
            '{{%idx-board_option_in_category-id_board_category}}',
            '{{%board_option_in_category}}'
        );

        $this->dropTable('{{%board_option_in_category}}');
    }
}
