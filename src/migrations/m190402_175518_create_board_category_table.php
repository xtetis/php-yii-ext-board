<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%board_category}}`.
 */
class m190402_175518_create_board_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%board_category}}', [
            'id' => $this->primaryKey(),
            'id_parent' => $this->integer()->defaultValue(0),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'active' => $this->integer()->defaultValue(1),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');


        // creates index for column `id_parent`
        $this->createIndex(
            '{{%idx-id_parent}}',
            '{{%board_category}}',
            'id_parent'
        );


        $this->addCommentOnColumn('{{%board_category}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%board_category}}','id_parent', 'Родительская категория');
        $this->addCommentOnColumn('{{%board_category}}','name', 'Название категории');
        $this->addCommentOnColumn('{{%board_category}}','active', 'Активность записи');
        $this->addCommentOnTable('{{%board_category}}','Список категорий объявлений');

        $board_category = array(
            array('id' => '1','id_parent' => NULL,'name' => 'ww','active' => '1'),
            array('id' => '2','id_parent' => NULL,'name' => '222','active' => '1')
          );
          

          foreach ($board_category as $v) {
            $this->insert('board_category', $v);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%board_category}}');
    }
}
